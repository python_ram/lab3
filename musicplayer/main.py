from kivy.core.audio import SoundLoader
from kivy.app import App

def_vol = .25 # so newly initiated soundloader takes the position value of slider as default value

class Sound(SoundLoader):
    	def __init__(self):
		global def_vol
		self.sound = Sound.load('Jesse_Cook-Dance_Of_Spring.ogg')
		#*****
		self.sound.volume = def_vol
		#*****
		print "soundtrack has been loaded"

sl_list = []


class HelloWorldApp(App):
    	def play_music(self):		
		global sl_list
		loaded = Sound()
		sl_list.append(loaded)
		print sl_list
		if sl_list[0].sound.state == 'stop': 
			loaded.sound.play()
			print loaded.sound.volume 
			if len(sl_list) == 2:
				sl_list.pop(0)
		else:
			sl_list.pop()
		print sl_list 
				
	def stop_music(self):
		if len(sl_list) == 1:
			sl_list[0].sound.stop()
		else:
			pass  #to avoid error when stop is clicked with no sound playing or initially
		
	def volume_control(self,vol):
		global def_vol
		if len(sl_list) == 0:
			def_vol = vol		
			pass
		else:
			print vol    # to view volume level
			def_vol = vol		
			sl_list[0].sound.volume = vol 		

if __name__=="__main__":
    	HelloWorldApp().run()

#final completion
#problem with smart.git commits

  
